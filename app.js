const express = require("express");
const bodyparser = require("body-parser");
const mongoose = require("mongoose");
const cors = require("cors");

const app = express();


app.use(cors());
app.use(bodyparser.json({ limit: "50mb" }));
app.use(
  bodyparser.urlencoded({
    limit: "50mb",
    extended: true,
    parameterLimit: 1000000,
  })
);

app.use(bodyparser.urlencoded({ extended: true }));

app.use(bodyparser.json());

app.use(express.static("public"));

const config = require("./constants/config");
const User = require("./routes/user");
const Products = require("./routes/product");
const Orders = require("./routes/order");

app.use("/api/user", User);
app.use("/api/products", Products);
app.use("/api/orders", Orders);


mongoose.connect(
    config.Db,
    { useNewUrlParser: true, useUnifiedTopology: true },
    function () {
      console.log("Db Connected!");
    }
  );
  
  app.listen(config.port);