const mongoose = require("mongoose");

const ProductSchema = mongoose.Schema({
  Productname: { type: String, required: true },
  description: { type: String, required: true },
  category: { type: String, required: true },
  unitprice: { type: Number, required: true },
  quantity: { type: Number, required: true },
  totalprice: { type: Number, required: true },
});

const OrderSchema = mongoose.Schema({
  orderdescription: { type: String, required: true },
  product: ProductSchema,
  totprice: { type: Number, required: true },
});

const Orders = mongoose.model("Order", OrderSchema);
exports.Orders = Orders;
