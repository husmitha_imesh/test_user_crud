const mongoose  = require('mongoose')

const userSchema = mongoose.Schema({
   name:  { type: String, required: true, unique: false },
   email:  { type: String, required: true, unique: true },
   password:  { type: String, required: true, unique: false },
   address:  { type: String, required: true, unique: false },
});

const User = mongoose.model("user", userSchema);
exports.User = User;