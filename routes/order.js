const express = require("express");
const router = express.Router();
const { User } = require("../models/user");
const { Orders } = require("../models/orders");
const { Product } = require("../models/product");

router.get("/all", async (req, res) => {
  try {
    let orderDetails = await Orders.find();

    if (!orderDetails) {
      return res.send({
        success: false,
        message: "No Order made",
      });
    } else {
      return res.send({
        success: true,
        message: "Order sccuessfully get",
        content: orderDetails,
      });
    }
  } catch (error) {
    console.log(error);
    return res.send({
      success: false,
      message: "Server error",
    });
  }
});
router.get("/single/:id", async (req, res) => {
  try {
    const [{ Productname, description, category, price }] = ProductSchema;

    // let Productname = req.body.Productname
    // let description =  req.body.description
    // let category =  req.body.category
    // let price =  req.body.price

    let orderDetails = await Orders.findById(req.params.id);

    if (!orderDetails) {
      return res.send({
        success: false,
        message: "No Order",
      });
    } else {
      return res.send({
        success: true,
        message: "Order sccuessfully get",
        content: orderDetails,
      });
    }
  } catch (error) {
    console.log(error);
    return res.send({
      success: false,
      message: "Server error",
    });
  }
});
router.post("/addOrder", async (req, res) => {
  // const user = await User.findOne({ email: req.body.email });

  // if (!user) {
  //   return res.send({
  //     success: false,
  //     message: "No Vendor",
  //   });
  // }

  try {
    // const product = await Product.findOne({ _id: req.body.id });

    // if (!product) {
    //   return res.send({
    //     success: false,
    //     message: "No Product",
    //   });
    // }

    
//quantity in product it means there are different amount in same product there should be net price means multiply 
//by quantity and unit price
//ther should be totlat price for orders as well
    const {mapProductItems} = req.body;

    const {Productname, description, category, price} = mapProductItems;

    let totprice = mapProductItems.price * req.body.quantity;

    const order = new Orders({
      orderdescription: req.body.description,
      product: mapProductItems,
      quantity: req.body.quantity,
      totprice: totprice,
    });

    await order.save();

    //reduce the count of inventory dont delete it
    let abc = await Product.findByIdAndDelete({_id: req.body.id})

    return res.send({
      success: true,
      message: "Order successfully added",
    });
  } catch (error) {
    console.log(error);
    return res.send({
      success: false,
      message: "Server error",
    });
  }
});
// router.delete("/delete/:id", async (req, res) => {});
// router.put("/updateOrder/:id", async (req, res) => {});

module.exports = router;
