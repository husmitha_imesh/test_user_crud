const express = require("express");
const router = express.Router();
const { Product } = require("../models/product");

router.get("/all", async (req, res) => {
  try {
    let productDetails = await Product.find();

    if (!productDetails) {
      return res.send({
        success: false,
        message: "No product",
      });
    } else {
      return res.send({
        success: true,
        message: "Product sccuessfully get",
        content: productDetails,
      });
    }
  } catch (error) {
    console.log(error);
    return res.send({
      success: false,
      message: "Server error",
    });
  }
});

router.get("/single/:id", async (req, res) => {
  try {
    let productDetails = await Product.findById(req.params.id);

    if (!productDetails) {
      return res.send({
        success: false,
        message: "No product",
      });
    } else {
      return res.send({
        success: true,
        message: "Product sccuessfully get",
        content: productDetails,
      });
    }
  } catch (error) {
    console.log(error);
    return res.send({
      success: false,
      message: "Server error",
    });
  }
});

router.post("/addProduct", async (req, res) => {
  
    try {

    const nProduct = new Product({
      Productname: req.body.Productname,
      description: req.body.description,
      category: req.body.category,
      quantity: req.body.quantity,
      price: req.body.price,
    });

    await nProduct.save();

    return res.send({
      success: true,
      message: "sccuessfully add product",
    });
  } catch (error) {
    console.log(error);
    return res.send({
      success: false,
      message: "Server error",
    });
  }
});

router.delete("/deleteProd/:id", async (req, res) => {
  const vendor = await User.findOne({ email: req.body.email });

  if (!vendor) {
    return res.send({
      success: false,
      message: "No Vendor",
    });
  } else {
    let productID = await Product.findByIdAndDelete(req.params.id);

    try {
      if (!productID) {
        return res.send({
          success: false,
          message: "No Product Found",
        });
      } else {
        return res.send({
          success: true,
          message: "sccuessfully delete product",
        });
      }
    } catch (error) {
      console.log(error);
      return res.send({
        success: false,
        message: "Server error",
      });
    }
  }
});

router.put("/updateProduct/:id", async (req, res) => {
  
    try {
      let productID = await Product.findById(req.params.id);

      if (!productID) {
        return res.send({
          success: false,
          message: "No product",
        });
      }

      productID = productID.set({
        Productname: req.body.Productname,
        description: req.body.description,
        category: req.body.category,
        quantity: req.body.quantity,
        price: req.body.price,
      });

      await productID.save();

      return res.send({
        success: true,
        message: "Successfully update Product",
      });
    } catch (error) {
      console.log(error);
      return res.send({
        success: false,
        message: "Server error",
      });
    }
});

module.exports = router;
