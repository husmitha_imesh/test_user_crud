const express = require("express");
const router = express.Router();
const jwt = require("jsonwebtoken");
const { TOKEN_SEC } = require("../constants/tokenKey");
const { encrypt } = require("../controllers/encrypt");
const { decrypt } = require("../controllers/decrypt");
const { User } = require("../models/user");

router.get("/all", async (req, res) => {
  try {
    let users = await User.find();

    if (!users) {
      return res.send({
        success: false,
        message: "No user Exists",
      });
    } else {
      return res.send({
        success: true,
        message: "user details",
        content: users,
      });
    }
  } catch (error) {
    console.log(error);
    return res.send({
      success: false,
      message: "Server error",
    });
  }
});

router.post("/add", async (req, res) => {
  try {
    let user = await User.findOne({ email: req.body.email });

    const encryptedPassword = encrypt(req.body.password);

    if (user) {
      return res.send({
        success: false,
        message: "User already Exists",
      });
    } else {
      user = new User({
        name: req.body.name,
        email: req.body.email,
        password: encryptedPassword,
        address: req.body.address,
      });

      await user.save();

    

      return res.send({
        success: true,
        message: "Successfully addedd user",
      });
    }
  } catch (error) {
    console.log(error);
    return res.send({
      success: false,
      message: "Server error",
    });
  }
});

router.delete("/delete/:id", async (req, res) => {
  try {
    let user = await User.findByIdAndRemove(req.params.id);

    if (!user) {
      return res.send({
        success: false,
        message: "No user exists to delete",
      });
    } else {
      return res.send({
        success: true,
        message: "Successfully deleted user",
      });
    }
  } catch (error) {
    console.log(error);
    return res.send({
      success: false,
      message: "Server error",
    });
  }
});

router.put("/updateUser/:id", async (req, res) => {
  try {
    let updateUser = await User.findById(req.params.id);

    const upEncryptedPassword = encrypt(req.body.password);

    if (!updateUser) {
      return res.send({
        success: false,
        message: "No user to update",
      });
    }

    updateUser = updateUser.set({
      name: req.body.name,
      email: req.body.Email,
      password: upEncryptedPassword,
      address: req.body.address,
    });

    await updateUser.save();

   

    return res.send({
      success: true,
      message: "Successfully Updated user",
    });
  } catch (error) {
    console.log(error);
    return res.send({
      success: false,
      message: "Unauthorised Access",
    });
  }
});

router.post("/login", async (req, res) => {
  try {
    const accessEmail = req.body.Email;
    const accessPassword = req.body.Password;

    let loggedUser = await User.findOne({ email: accessEmail });

    if (!loggedUser) {
      return res.send({
        success: false,
        message: "No user",
      });
    }

    const DbPasword = decrypt(loggedUser.password);

    if (DbPasword == accessPassword && loggedUser.email == accessEmail) {
      const tokenPayload = {
        userId: loggedUser._id,
        email: loggedUser.email,
        firstName: loggedUser.name,
        address: loggedUser.address,
      };

      const token1 = jwt.sign(tokenPayload, TOKEN_SEC);

      return res.send({
        success: true,
        message: "Successfully Login user",
        Token: token1,
      });
    } else {
      return res.send({
        success: false,
        message: "Password or Email mismatch",
      });
    }
  } catch (error) {
    console.log(error);
    return res.send({
      success: false,
      message: "Server error",
    });
  }
});

module.exports = router;
