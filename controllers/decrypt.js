var crypto = require("crypto");
const {key} = require("../constants/passwordConfig")

function decrypt (password) {
    var decipher = crypto.createDecipher('aes-256-cbc', key);
    var decrypted = decipher.update(password, 'hex', 'utf-8');
    decrypted += decipher.final('utf-8');

    return decrypted;
}

module.exports = {decrypt}
