var crypto = require("crypto");
const {key} = require("../constants/passwordConfig")

//encription key
// var key = "krampencriptionkey+-$$-E112";

 function encrypt (data) {
    var cipher = crypto.createCipher('aes-256-cbc', key);
    var crypted = cipher.update(data, 'utf-8', 'hex');
    crypted += cipher.final('hex');

    return crypted;
}

module.exports = {encrypt}

